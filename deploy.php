<?php

namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'Catalog');

set('repository', 'git@bitbucket.org:cybercatza/l56-laratrust.git');

host('196.38.48.211')
    ->user('root')
    ->port(2222)
	->stage('production')
    ->set('branch', 'master')
	->set('deploy_path', '/var/www/outsourcing-solutions/'); // Define the base path to deploy your project to.

set('keep_releases', 5);
set('git_tty', true);

add('shared_dirs', ['storage']);
add('shared_files', ['.env']);

set('writable_mode', 'chown');
set('writable_use_sudo', false);

// Writable dirs by web server
add('writable_dirs', ['bootstrap/cache']);
set('allow_anonymous_stats', false);

// Specify the repository from which to download your project's code.
// The server needs to have git installed for this to work.
// If you're not using a forward agent, then the server has to be able to clone
// your project from this repository.

// Clear the user profiles in cache,
// in case there were changes in the auth-related code
task('profiles', function () {
	run('php {{deploy_path}}/current/artisan cache:clear');
});

